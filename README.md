# Metroplex 2.0.1

> Powerful project skeleton utilizing Grunt.

[![devDependency Status](https://david-dm.org/metropoly/Metroplex/initialised/dev-status.svg)](https://david-dm.org/metropoly/Metroplex/initialised#info=devDependencies)
[![Built with Grunt](https://cdn.gruntjs.com/builtwith.png)](http://gruntjs.com/)

## Overview
Metroplex is a basic project skeleton built by [Metropoly](http://metropoly.co.uk) as their project wireframe. It is built around Grunt and uses some of the latest technologies in web development.

## Quick Start
- Clone the repo
- `grunt-init metroplex` in your new project folder
- Install the dependenceis with `npm install`
- Start writing code, compile and watch for changes with `grunt`

## In-depth installation

### Requirements
Install the following items on your machine globally.

 - [Node JS](http://nodejs.org/)
 - [npm](https://www.npmjs.com/)
 - [Grunt CLI](http://gruntjs.com/getting-started)
 - [grunt-init](https://github.com/gruntjs/grunt-init)

### Step by step
 - Clone the repo into the grunt-init folder, most likely to be in `~/.grunt-init`
 - `cd` to your project folder, ie "my-project"
 - When in "my-project", run `grunt-init metroplex` and follow the prompts that appear in the terminal
 - After the project has finished building, run `npm install` - this may take a while
 - Start writing code, and compile using `grunt`, this will also start watching for changes.

### Usage
 - SASS/SCSS goes in `src/sass` and compiles to `src/css`
 - JS goes in `src/js_src` and compiles to `src/js`
 - Images go in `src/images_src` and compress to `src/images`

### The grunt tasks
There are several grunt tasks, this is a rough breakdown

##### `grunt sass`
`grunt sass` runs the default css tasks. It takes all of the scss files in `src/sass` and compiles them using [grunt-sass](https://www.npmjs.com/package/grunt-sass). It then [grunt-autoprefixer](https://www.npmjs.com/package/grunt-autoprefixer). your style properties with vendor prefixes. Autoprefixer is setup to cover browsers which fall into thse catagories; >5% usage, last 2 versions, IE9, Opera 12, Firefox 15, Chrome 25, Safari 6.

##### `grunt js`
`grunt js` runs the defult js tasks. First it lints your `src/js_src/app.js` file using [grunt-contrib-jshint](https://www.npmjs.com/package/grunt-contrib-jshint). Then [grunt-contrib-concat](https://www.npmjs.com/package/gulp-concat) takes all files with the prefix of `_` and bundles it with your app.js file and saves it into a sinlge app.js file `src/js/app.js`

##### `grunt images`
`grunt images` runs the default images task. Using [grunt-contrib-imagemin](https://www.npmjs.com/package/grunt-contrib-imagemin), all your images within the `src/images_src` folder are compressed to save on file size and moved to `src/images`.

##### `grunt` or `grunt default`
`grunt` is the default project task. Running this will run the three default tasks, `grunt sass`, `grunt js`, and `grunt images`. This task will then watch for changes using [grunt-contrib-watch](https://www.npmjs.com/package/grunt-contrib-watch), and continue to run tasks as required.

#### `grunt fast`
`grunt fast` is similar to the default `grunt` task, but it doesn't watch your project for any changes.

##### `grunt build` and `grunt build:release`
When you get to the end of the project, it's important to be able to hand the correct files to the client. Simply run `grunt build` have a `build` folder created, with all of the compiled JS, CSS, and images. Running `grunt build:relase` will also compress the code.

#### `grunt deploy`
`grunt deploy` is the task used for deploying your code to a server, this can be especially helpful if you need to show a client your work on a test/dev domain.
 - Open `ftp+config.json` and fill out the correct information, in the following format;
```
{
	"host": "ftp.host.com",
	"username": "my-name",
	"password": "password123",
	"port": "21",
	"path": "dev/my-project"
}
```
After that, run task `grunt deploy`. This will `grunt build:release` your project, and then transfer the files to your server path using [grunt-ftpush](https://www.npmjs.com/package/grunt-ftpush). When you run it again in the future it will only transfer changed and new files.

###### Made by Metropoly :)