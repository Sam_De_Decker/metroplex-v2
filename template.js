'use strict';

exports.description = 'Create the Metropoly default project.';

exports.notes = 'Powerful project skeleton';

exports.after = "You're almost done, you just need to run the following \n\n" +
	"npm install \n\n" +
	"Now you're done!" +
	"\nThe Metropoly Team";

exports.warnOn = '*';

exports.template = function(grunt, init, done) {

	init.process({
			licenses: []
		}, [
			init.prompt('name'),
			init.prompt('description', 'The next big thing'),
			init.prompt('version', '1.0.0'),
		],
		function(err, props) {
			props.keywords = [];
			props.devDependencies = {
				"grunt": "^0.4.5",
    			"grunt-autoprefixer": "^3.0.0",
    			"grunt-contrib-clean": "^0.6.0",
    			"grunt-contrib-concat": "^0.5.1",
    			"grunt-contrib-copy": "^0.8.0",
    			"grunt-contrib-imagemin": "^0.9.4",
    			"grunt-contrib-jshint": "^0.11.2",
    			"grunt-contrib-uglify": "^0.9.1",
    			"grunt-contrib-watch": "^0.6.1",
    			"grunt-ftpush": "^0.3.3",
    			"grunt-newer": "^1.1.0",
    			"grunt-sass": "^1.0.0"
			};

			var noProcess = {
				noProcess: ['src/css/fonts/*']
			};

			var files = init.filesToCopy(props);
			//init.addLicenseFiles(files, props.licenses);
			init.copyAndProcess(files, props, noProcess);
			init.writePackageJSON('package.json', props);

			done();
		});
};