'use strict'

module.exports = function(grunt) {

	var ftpconfig = grunt.file.readJSON('ftp_config.json')

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		watch: {
			sass: {
				files: ['src/sass/**/*.{scss,sass}'],
				tasks: ['sass:dev', 'autoprefixer']
			},
			js: {
				files: ['src/js_src/**/*.js'],
				tasks: ['jshint:beforeconcat', 'concat:dev']
			},
			imagemin: {
				files: ['images_src/**/*.{png,jpg,jpeg,gif,svg}']
			}
		},
		autoprefixer: {
			options: {
				// We need to `freeze` browsers versions for testing purposes.
				browsers: ['> 5%', 'last 2 versions', 'ie 9', 'opera 12', 'ff 15', 'chrome 25', 'safari 6']
			},
			multiple_files: {
				expand: true,
				flatten: true,
				src: 'src/css/*.css',
				dest: 'src/css/'
			}
		},
		concat: {
			options: {
				seperator: ";"
			},
			dev: {
				src: ['src/js_src/**/_*.js', 'src/js_src/app.js'],
				dest: 'src/js/app.js',
				nonull: true,
				sourceMap: true
			}
		},
		clean: ['build/*'],
		copy: {
			debug: {
				expand: true,
				cwd: 'src/',
				src: ['**/*', '!.sass-cache/*', '!sass', '!sass/**', '!js/app.js', '!js_src/**', '!images_src/**'],
				dest: 'build/',
				nonull: true
			},
			release: {
				expand: true,
				cwd: 'src/',
				src: ['**/*', '!.sass-cache/*', '!sass', '!sass/**', '!js/app.js', '!js_src/**', '!images_src/**'],
				dest: 'build/',
				nonull: true
			}
		},
		ftpush: {
  			build: {
    			auth: {
    			  host: ftpconfig.host,
    			  port: ftpconfig.port,
    			  username: ftpconfig.username,
    			  password: ftpconfig.password
    			},
    			src: 'build',
    			dest: ftpconfig.path,
    			exclusions: ['build/**/.DS_Store', 'build/**/Thumbs.db', 'src/*'],
    			simple: true,
    			useList: false
  			}
		},
		imagemin: {
			options: {
				optimizationLevel: 3,
				svgoPlugins: [{ removeViewBox: false }],
			},
			dist: {
				files: [{
					expand: true,
					cwd: 'src/images_src',
					src: ['**/*.{png,jpg,jpeg,gif,svg}'],
					dest: 'src/images'
				}]
			}
		},
		jshint: {
			options: {
				asi: true,
				curly: true,
				eqnull: true,
				eqeqeq: true,
				nonbsp: true,
				undef: true,
				unused: true,
				browser: true,
				devel: true,
				jquery: true,
				force: true
			},
			beforeconcat: ['src/js_src/app.js'],
			afterconcat: ['src/js/app.js']
		},
		sass: {
			options: {
				precision: 10,
				sourceMap: true,
				sourceComments: true
			},
			dev: {
				options: {
					outputStyle: 'nested'
				},
				files: {
					'src/css/styles.css': 'src/sass/styles.scss'
				}
			},
			release: {
				options: {
					outputStyle: 'compressed'
				},
				files: {
					'src/css/styles.css': 'src/sass/styles.scss'
				}
			}
		},
		uglify: {
			options: {
				mangle: false,
				compress: true,
				sourceMap: true,
				preserveComments: false
			},
			release: {
				files: {
					'build/js/app.js': ['src/js/app.js']
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-ftpush');
	grunt.loadNpmTasks('grunt-newer');
	grunt.loadNpmTasks('grunt-sass');

	grunt.registerTask('default', ['fast', 'watch']);
	grunt.registerTask('fast', ['css', 'js', 'images']); // No watch
	grunt.registerTask('css', ['sass:dev', 'autoprefixer']);
	grunt.registerTask('js', ['jshint:beforeconcat', 'concat:dev']);
	grunt.registerTask('images', ['newer:imagemin:dist']);

	grunt.registerTask('build', ['clean', 'sass:dev', 'autoprefixer', 'js', 'images', 'copy:debug']);
	grunt.registerTask('build:release', ['clean', 'sass:release', 'autoprefixer', 'js', 'uglify:release', 'images', 'copy:release']);

	grunt.registerTask('deploy', ['build:release', 'ftpush']);
	grunt.registerTask('deploy:debug', ['build', 'ftpush']);
}